module gitlab.com/go-auth/postgres

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/jackc/pgx/v4 v4.8.1
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/go-auth/goauth v0.0.6
)
