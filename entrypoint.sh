#!/bin/sh
set -e
cmd="$@"

wait_for_service() {
  host="$1"
  port="$2"

  printf "Waiting for ${host}:${port} ... "
  while ! nc -z $host $port; do
    sleep 0.5
  done
  printf "done\n"
}

apk add gcc libc-dev
wait_for_service postgres 5432

exec $cmd

