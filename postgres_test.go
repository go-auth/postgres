package postgres

import (
	"context"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/suite"
)

type testSuite struct {
	suite.Suite
	store *Store
}

func (s *testSuite) SetupSuite() {
	godotenv.Load()
	s.store = New(os.Getenv("DATABASE_URL"))
}

func (s *testSuite) TearDownTest() {
	_, err := s.store.connection.Exec(context.Background(), "TRUNCATE account CASCADE")
	if err != nil {
		panic(err)
	}
}

func (s *testSuite) TearDownSuite() {
	tables := strings.Join([]string{
		"account",
		"authorization_code",
		"access_token",
		"refresh_token",
	}, ",")

	_, err := s.store.connection.Query(context.Background(), "DROP TABLE IF EXISTS "+tables)
	if err != nil {
		panic(err)
	}
}

func (s *testSuite) TestUpsertAccount() {
	t := s.Assert()
	acc1, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)
	t.Equal("google", acc1.Platform)
	t.Equal("1234", acc1.PlatformID)
	t.Equal("test@gmail.com", acc1.Email)
	t.Equal("tester", acc1.Name)

	acc2, err := s.store.UpsertAccount("1234", "google", "new@gmail.com", "tester")
	t.Nil(err)
	t.Equal(acc1.ID, acc2.ID)
	t.Equal(acc1.Name, acc2.Name)
	t.Equal("new@gmail.com", acc2.Email)
}

func (s *testSuite) TestGenerateAuthorizationCode() {
	t := s.Assert()
	acc, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)

	authCode1, err := s.store.GenerateAuthorizationCode(acc.ID, "test", "plain")
	t.Nil(err)

	t.NotEqual("", authCode1.Code)
	t.Equal(acc.ID, authCode1.AccountID)

	authCode2, err := s.store.GenerateAuthorizationCode(acc.ID, "test", "plain")
	t.Nil(err)

	t.NotEqual("", authCode2.Code)
	t.Equal(acc.ID, authCode2.AccountID)
	t.NotEqual(authCode1.Code, authCode2.Code)
}

func (s *testSuite) TestGetAuthorizationCode() {
	t := s.Assert()
	acc, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)

	authCode1, err := s.store.GenerateAuthorizationCode(acc.ID, "test", "plain")
	t.Nil(err)

	code := authCode1.Code

	authCode2, err := s.store.GetAuthorizationCode(code)
	t.Nil(err)
	t.Equal(authCode2.Code, authCode1.Code)

	t.Equal("", authCode2.ConvertedToToken)

	_, err = s.store.GetAuthorizationCode("invalid")
	t.Equal("no rows in result set", err.Error())
}

func (s *testSuite) TestExchangeAuthorizationCode() {
	t := s.Assert()
	acc, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)

	authCode, err := s.store.GenerateAuthorizationCode(acc.ID, "test", "plain")
	t.Nil(err)

	accessToken, refreshToken, err := s.store.ExchangeAuthorizationCode(authCode.Code)
	t.Nil(err)

	t.Equal(acc.ID, accessToken.AccountID)
	t.True(accessToken.ExpiresAt.After(time.Now()))
	t.Equal(accessToken.ID, refreshToken.AccessTokenID)

	usedAuthCode, err := s.store.GetAuthorizationCode(authCode.Code)
	t.Nil(err)
	t.Equal(accessToken.ID, usedAuthCode.ConvertedToToken)

	// exchanged code can't be used again
	_, _, err = s.store.ExchangeAuthorizationCode(authCode.Code)
	t.Equal("no rows in result set", err.Error())
}

func (s *testSuite) TestGetRefreshToken() {
	t := s.Assert()
	acc, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)

	authCode, err := s.store.GenerateAuthorizationCode(acc.ID, "test", "plain")
	t.Nil(err)

	_, refreshToken, err := s.store.ExchangeAuthorizationCode(authCode.Code)
	t.Nil(err)

	refreshTokenFromDB, err := s.store.GetRefreshToken(refreshToken.Code)
	t.Nil(err)

	t.EqualValues(refreshToken, refreshTokenFromDB)
}

func (s *testSuite) TestRefreshAccessToken() {
	t := s.Assert()
	acc, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)

	authCode, err := s.store.GenerateAuthorizationCode(acc.ID, "test", "plain")
	t.Nil(err)

	accessToken, refreshToken, err := s.store.ExchangeAuthorizationCode(authCode.Code)
	t.Nil(err)

	newAccessToken, err := s.store.RefreshAccessToken(refreshToken.Code)
	t.Nil(err)
	t.NotEqual(accessToken.Code, newAccessToken.Code)
	t.NotEqual(accessToken.ExpiresAt, newAccessToken.ExpiresAt)

	// invalid refresh token
	_, err = s.store.RefreshAccessToken("invalid")
	t.Equal("no rows in result set", err.Error())
}

func (s *testSuite) TestGetAccountByID() {
	t := s.Assert()
	acc, err := s.store.UpsertAccount("1234", "google", "test@gmail.com", "tester")
	t.Nil(err)

	acc, err = s.store.GetAccountByID(acc.ID)
	t.Nil(err)

	t.Equal(acc.Platform, "google")
	t.Equal(acc.PlatformID, "1234")
	t.Equal(acc.Email, "test@gmail.com")
	t.Equal(acc.Name, "tester")
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(testSuite))
}
