FROM golang:1.14-alpine

WORKDIR /src

COPY go.mod /src/go.mod
COPY go.sum /src/go.sum

RUN go mod download
RUN apk add --no-cache --update gcc libc-dev

ENTRYPOINT [ "/src/entrypoint.sh" ]
