package postgres

import (
	"context"
	"database/sql"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/go-auth/goauth"
)

// migrations should be idempotent because they are executed everytime New() is called
var migrations = []string{
	`
		CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
		CREATE TABLE IF NOT EXISTS account (
			id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
			platform_id TEXT NOT NULL,
			platform TEXT NOT NULL,
			name TEXT NOT NULL,
			email TEXT NOT NULL,
			created_at TIMESTAMPTZ DEFAULT NOW(),
			updated_at TIMESTAMPTZ DEFAULT NOW(),

			UNIQUE(platform_id, platform)
		);

		CREATE TABLE IF NOT EXISTS access_token (
			id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
			account_id UUID NOT NULL REFERENCES account(id) ON DELETE CASCADE ON UPDATE CASCADE,
			code TEXT NOT NULL,
			scopes TEXT[] DEFAULT '{}',
			expires_at TIMESTAMPTZ DEFAULT NOW() + '24 hours'::interval,
			created_at TIMESTAMPTZ DEFAULT NOW(),
			updated_at TIMESTAMPTZ DEFAULT NOW(),

			UNIQUE(code)
		);

		CREATE TABLE IF NOT EXISTS authorization_code (
			id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
			account_id UUID NOT NULL REFERENCES account(id) ON DELETE CASCADE ON UPDATE CASCADE,
			code TEXT NOT NULL,
			code_challenge TEXT NOT NULL,
			code_challenge_method TEXT NOT NULL,
			created_at TIMESTAMPTZ DEFAULT NOW(),
			updated_at TIMESTAMPTZ DEFAULT NOW(),
			converted_to_token UUID REFERENCES access_token(id) ON DELETE CASCADE DEFAULT NULL,

			UNIQUE(code),
			UNIQUE(converted_to_token)
		);

		CREATE TABLE IF NOT EXISTS refresh_token (
			id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
			access_token_id UUID NOT NULL REFERENCES access_token(id) ON DELETE CASCADE ON UPDATE CASCADE,
			code TEXT NOT NULL,
			created_at TIMESTAMPTZ DEFAULT NOW(),
			updated_at TIMESTAMPTZ DEFAULT NOW(),

			UNIQUE(code)
		);
	`,
}

// New returns a new Store instance. The databaseURL is in form of postgres://username:password@host:port/database?option=value
func New(databaseURL string) *Store {
	ctx := context.Background()
	conn, err := pgxpool.Connect(ctx, databaseURL)

	if err != nil {
		panic(err)
	}

	tx, err := conn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		panic(err)
	}

	for _, migration := range migrations {
		_, err = tx.Exec(ctx, migration)
		if err != nil {
			tx.Rollback(ctx)
			panic(err)
		}
	}
	err = tx.Commit(ctx)
	if err != nil {
		panic(err)
	}

	return &Store{conn}
}

// Store implements goauth.Store interface based on postgres
type Store struct {
	connection *pgxpool.Pool
}

func (s *Store) generateCode() (string, error) {
	uuid, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}

	return strings.ReplaceAll(uuid.String(), "-", ""), nil
}

// UpsertAccount inserts or updates an account. Accounts are unique based on their (platform, platformID)
// Same email from 2 different platforms are valid
func (s *Store) UpsertAccount(platformID string, platform string, email string, name string) (*goauth.Account, error) {
	query := `
		INSERT INTO account (platform_id, platform, email, name) VALUES (
			$1, $2, $3, $4
		) ON CONFLICT (platform_id, platform) DO UPDATE 
		SET email = EXCLUDED.email,
				name = EXCLUDED.name,
				updated_at = NOW()
		RETURNING id, email, name, created_at, updated_at
	`

	var id string
	var createdAt time.Time
	var updatedAt time.Time
	var actualName string
	var actualEmail string

	err := s.connection.
		QueryRow(context.Background(), query, platformID, platform, email, name).
		Scan(&id, &actualEmail, &actualName, &createdAt, &updatedAt)
	if err != nil {
		return nil, err
	}

	return &goauth.Account{
		ID:         id,
		PlatformID: platformID,
		Platform:   platform,
		Email:      actualEmail,
		Name:       actualName,
		CreatedAt:  createdAt,
		UpdatedAt:  updatedAt,
	}, nil
}

// GenerateAuthorizationCode generates an authorization code associated with the challenge provided
// This follows PKCE spec
func (s *Store) GenerateAuthorizationCode(accountID string, challenge string, challengeMethod string) (*goauth.AuthorizationCode, error) {
	code, err := s.generateCode()
	if err != nil {
		return nil, err
	}

	var id string
	var createdAt time.Time
	var updatedAt time.Time

	query := `
		INSERT INTO authorization_code (account_id, code, code_challenge, code_challenge_method) VALUES (
			$1, $2, $3, $4
		) RETURNING id, created_at, updated_at
	`

	err = s.connection.
		QueryRow(context.Background(), query, accountID, code, challenge, challengeMethod).
		Scan(&id, &createdAt, &updatedAt)
	if err != nil {
		return nil, err
	}

	return &goauth.AuthorizationCode{
		ID:                  id,
		AccountID:           accountID,
		Code:                code,
		CodeChallenge:       challenge,
		CodeChallengeMethod: challengeMethod,
		CreatedAt:           createdAt,
		UpdatedAt:           updatedAt,
	}, nil
}

// GetAuthorizationCode returns an authorization code
func (s *Store) GetAuthorizationCode(code string) (*goauth.AuthorizationCode, error) {
	query := `
		SELECT id,
			account_id,
			code_challenge,
			code_challenge_method,
			created_at,
			updated_at,
			converted_to_token
		FROM authorization_code
		WHERE code = $1
	`

	var id, accountID, codeChallenge, codeChallengeMethod string
	var convertedToToken sql.NullString
	var createdAt, updatedAt time.Time

	err := s.connection.
		QueryRow(context.Background(), query, code).
		Scan(&id, &accountID, &codeChallenge, &codeChallengeMethod, &createdAt, &updatedAt, &convertedToToken)
	if err != nil {
		return nil, err
	}

	return &goauth.AuthorizationCode{
		ID:                  id,
		AccountID:           accountID,
		Code:                code,
		CodeChallenge:       codeChallenge,
		CodeChallengeMethod: codeChallengeMethod,
		CreatedAt:           createdAt,
		UpdatedAt:           updatedAt,
		ConvertedToToken:    convertedToToken.String,
	}, nil
}

// ExchangeAuthorizationCode exchanges the authorization code for an access and refresh token
// This also marks the authoziation code as used and it can't be used again
func (s *Store) ExchangeAuthorizationCode(code string) (*goauth.AccessToken, *goauth.RefreshToken, error) {
	accessTokenCode, err := s.generateCode()
	if err != nil {
		return nil, nil, err
	}
	refreshTokenCode, err := s.generateCode()
	if err != nil {
		return nil, nil, err
	}

	accessToken := &goauth.AccessToken{
		Code: accessTokenCode,
	}

	refreshToken := &goauth.RefreshToken{
		Code: refreshTokenCode,
	}

	ctx := context.Background()

	tx, err := s.connection.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return nil, nil, err
	}

	var authCodeID, accountID string

	err = tx.QueryRow(ctx, `
		SELECT id, account_id FROM authorization_code WHERE code = $1 AND converted_to_token IS NULL
		FOR UPDATE
	`, code).Scan(&authCodeID, &accountID)

	if err != nil {
		tx.Rollback(ctx)
		return nil, nil, err
	}

	// no scopes for now
	err = tx.QueryRow(ctx, `
		INSERT INTO access_token(account_id, code, scopes, expires_at)
		VALUES ($1, $2, '{}', NOW() + '1 day'::interval)
		RETURNING id, account_id, expires_at, created_at, updated_at
	`, accountID, accessToken.Code).Scan(
		&accessToken.ID,
		&accessToken.AccountID,
		&accessToken.ExpiresAt,
		&accessToken.CreatedAt,
		&accessToken.UpdatedAt,
	)

	if err != nil {
		tx.Rollback(ctx)
		return nil, nil, err
	}

	err = tx.QueryRow(ctx, `
		INSERT INTO refresh_token(access_token_id, code)
		VALUES ($1, $2)
		RETURNING id, access_token_id, created_at, updated_at
	`, accessToken.ID, refreshToken.Code).Scan(
		&refreshToken.ID,
		&refreshToken.AccessTokenID,
		&refreshToken.CreatedAt,
		&refreshToken.UpdatedAt,
	)
	if err != nil {
		tx.Rollback(ctx)
		return nil, nil, err
	}

	_, err = tx.Exec(ctx, `
		UPDATE authorization_code SET converted_to_token = $1 WHERE id = $2
	`, accessToken.ID, authCodeID)

	if err != nil {
		tx.Rollback(ctx)
		return nil, nil, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		tx.Rollback(ctx)
		return nil, nil, err
	}

	return accessToken, refreshToken, nil
}

// GetRefreshToken returns a refresh token
func (s *Store) GetRefreshToken(code string) (*goauth.RefreshToken, error) {
	refreshToken := &goauth.RefreshToken{
		Code: code,
	}

	err := s.connection.QueryRow(context.Background(), `
		SELECT id, access_token_id, created_at, updated_at
		FROM refresh_token
		WHERE code = $1
	`, code).Scan(
		&refreshToken.ID,
		&refreshToken.AccessTokenID,
		&refreshToken.CreatedAt,
		&refreshToken.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return refreshToken, nil
}

// RefreshAccessToken refreshes its associated access token
func (s *Store) RefreshAccessToken(refreshTokenCode string) (*goauth.AccessToken, error) {
	ctx := context.Background()

	tx, err := s.connection.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return nil, err
	}

	accessToken := &goauth.AccessToken{}
	var accessTokenID string

	err = tx.QueryRow(ctx, `
		SELECT access_token_id FROM refresh_token WHERE code = $1
	`, refreshTokenCode).Scan(&accessTokenID)

	if err != nil {
		tx.Rollback(ctx)
		return nil, err
	}

	newAccessTokenCode, err := s.generateCode()
	if err != nil {
		tx.Rollback(ctx)
		return nil, err
	}

	err = tx.QueryRow(ctx, `
		UPDATE access_token SET
			code = $1,
			expires_at = NOW() + '1 day'::interval,
			updated_at = NOW()
		WHERE id = $2
		RETURNING code, expires_at, created_at, updated_at, account_id
	`, newAccessTokenCode, accessTokenID).Scan(
		&accessToken.Code,
		&accessToken.ExpiresAt,
		&accessToken.CreatedAt,
		&accessToken.UpdatedAt,
		&accessToken.AccountID,
	)

	if err != nil {
		tx.Rollback(ctx)
		return nil, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		tx.Rollback(ctx)
		return nil, err
	}

	return accessToken, nil
}

// GetAccountByID returns the account entry based on the provided id
func (s *Store) GetAccountByID(id string) (*goauth.Account, error) {
	account := &goauth.Account{}

	err := s.connection.QueryRow(context.Background(), `
		SELECT
			id,
			platform_id,
			platform,
			name,
			email,
			created_at,
			updated_at
		FROM account WHERE id = $1
	`, id).Scan(
		&account.ID,
		&account.PlatformID,
		&account.Platform,
		&account.Name,
		&account.Email,
		&account.CreatedAt,
		&account.UpdatedAt,
	)

	return account, err
}

// Close frees all resources
func (s *Store) Close(ctx context.Context) error {
	s.connection.Close()
	return nil
}
